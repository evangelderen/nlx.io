// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { ThemeProvider } from 'styled-components'
import { elementType, object } from 'prop-types'
import Head from 'next/head'
import { GlobalStyles } from '@commonground/design-system'
import '@fontsource/source-sans-pro/latin.css'
import theme from 'src/styling/theme'
import { MediaProvider } from 'src/styling/media'

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <MediaProvider>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <GlobalStyles />
        <Component {...pageProps} />
      </MediaProvider>
    </ThemeProvider>
  )
}

MyApp.propTypes = {
  Component: elementType,
  pageProps: object,
}

export default MyApp
