// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { getPageSections } from 'src/lib/api'
import Features from 'src/pages/Features'

export default function FeaturesPage(props) {
  return <Features {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('features')
  return { props: { sections } }
}
