// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { getPageSections } from 'src/lib/api'
import Contact from 'src/pages/Contact'

export default function ContactPage(props) {
  return <Contact {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('contact')
  return { props: { sections } }
}
