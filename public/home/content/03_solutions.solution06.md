---
icon: openArm
---

### Draagt bij aan een open markt

De software wordt open source ontwikkeld samen met gemeenten en leveranciers. De code is transparant. Verbeteringen zijn voor alle gebruikers beschikbaar. Ook marktpartijen kunnen toepassingen ontwikkelen die gebruikmaken van deze software. Daardoor draagt NLX bij aan een open markt.  
