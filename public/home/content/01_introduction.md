---
siteTitle: Wissel snel, veilig en AVG-proof gegevens uit binnen en tussen (overheids)organisaties
siteSubTitle: NLX is een open source API gateway ontwikkeld door de Common Ground community met ondersteuning van VNG
videoCaption: NLX in 1,5 minuut
videoId: gWfdtRCFPNo
videoCookieDisclaimer: Als u de video afspeelt, gaat u akkoord met het plaatsen van youtube cookies
---
