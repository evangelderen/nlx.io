---
icon: plug
---

### Efficiënter koppelingen beheren

NLX kan grootschalig gebruik van koppelingen op een standaard manier beheren en automatiseren. 
(Overheids)organisaties kunnen met NLX eenvoudig en snel een onderlinge verbinding voor gegevensuitwisseling opzetten. NLX kan worden ingezet binnen overheden, tussen overheden en tussen overheden & ketenpartners.
