---
title: Hoe werkt het?
introText: Iedere NLX deelnemer heeft een eigen NLX gateway binnen zijn organisatie. Deze gateway maakt zodra dat nodig is een directe verbinding met de NLX gateway van de andere organisatie. Over deze verbinding worden gegevens veilig opgehaald alsof deze gegevens lokaal aanwezig waren.
# If no value given, buttons won't render
button1Text: Technische documentatie
button1Link: https://docs.nlx.io
button2Text:
button2Link:
# Edit diagram text in SVG files
diagramA11yText: Het diagram toont een informatiestroom tussen twee organisaties die NLX als gateway gebruiken.
diagramFeaturesLinkHref: /features
---

### In de praktijk

Medewerker John gebruikt een applicatie en heeft gegevens nodig van een externe organisatie via NLX. De applicatie controleert of John wel de juiste rechten heeft om die gegevens in te zien (authenticatie en autorisatie bij de eigen organisatie). Wanneer John de juiste rechten heeft, controleert de lokale NLX gateway of er toestemming gegeven is door de externe organisatie (autorisatie) om de gegevens in te zien. Wanneer dat het geval is, voegt de lokale NLX gateway van de eigen organisatie het geldige organisatie PKIoverheid-certificaat toe (authenticatie) aan de vraag richting de externe organisatie. Deze vraag wordt bij de eigen organisatie gelogd. De vraag wordt via een directe verbinding verstuurd.

Bij de externe organisatie wordt door haar lokale NLX gateway wederom gecontroleerd of er toestemming is gegeven aan de vragende organisatie. Afhankelijk daarvan wordt het antwoord verstuurd. De vraag wordt ook hier gelogd. Het antwoord wordt ook gelogd bij beide organisaties. NLX logt alleen het verkeer, niet de inhoud van de berichten.
