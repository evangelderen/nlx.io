---
# If no value given, buttons won't render
button1Text: Lees meer over NLX
button1Link: /features
button2Text:
button2Link:
---

## Hoe lost NLX dit op?

NLX is een stelsel van software en afspraken die de gegevens van (overheids)organisaties onderling verbindt. Hiermee wordt het makkelijker om alleen gegevens rechtstreeks bij de bron op te halen en silo's te doorbreken, een flexibeler landschap te creëren waardoor beter ingespeeld kan worden op veranderende maatschappelijke vraagstukken en verlaging van beheerslast door simpeler beheer van koppelingen.
