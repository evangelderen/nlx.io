---
icon: shieldCheck
---

### Veilig

Voor alle gegevensuitwisseling gebruikt NLX standaard internettechnieken voor versleuteling, zoals wordt voorgeschreven door het Nationaal Cyber Security Center. Deze versleuteling is gebaseerd op PKIoverheid-certificaten en zorgt ervoor dat de gegevens tijdens het verzenden onleesbaar blijven. NLX dwingt af dat iedere organisatie altijd alleen zijn eigen PKIoverheid-certificaat nodig heeft. Hierdoor is altijd vast te stellen wie wat gedaan heeft. Door autorisatie verantwoordelijkheden helder en correct inzichtelijk te maken, zorgt NLX dat beheerslast af- en de veiligheid toeneemt.
