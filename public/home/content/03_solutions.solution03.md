---
icon: hammer
---

### Robuust

NLX is ontworpen als decentraal stelsel en bestaat uit losstaande modules die draaien bij de (overheids)organisaties die NLX gebruiken. Dat vermindert de kwetsbaarheid. Als een module uitvalt, blijft de rest het gewoon doen. Door de decentrale opzet kan NLX ook snel op- en afgeschaald worden.
