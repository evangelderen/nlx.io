---
symptoms:
  - icon: group
    text: Moeilijk inspelen op veranderende maatschappij
  - icon: fileCopy
    text: Grote kans op foutieve data door gebruik kopie administraties
  - icon: plug
    text: Grote diversiteit aan koppelingen vraag om specialistisch beheer
  - icon: building
    text: Werken in silo's waardoor hergebruik van data moeilijk is
---

## De informatievoorziening van Nederlandse overheden is door de jaren heen steeds complexer geworden. Het gevolg?
