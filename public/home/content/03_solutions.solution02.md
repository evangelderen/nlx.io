---
icon: lightning
---

### Snel

NLX is geoptimaliseerd voor snelle gegevensverwerking. Er is gekozen voor een programmeertaal specifiek voor snelle uitwisseling van gegevens. Ook bij de inrichting van NLX is gekozen voor snelheid. Controles die relatief veel tijd kosten (logging en autorisatie) vinden plaats op het lokale netwerk van de (overheids)organisatie. Het externe netwerk wordt gebruikt voor het ophalen van gegevens en de controle van het PKIoverheid-certificaat.
