---
mainLinkText: Bekijk volledige groeipact op CommonGround.nl
mainLinkHref: https://commonground.nl/cms/view/12f73f0d-ae26-4021-ba52-849eef37d11f/de-common-ground-principes/e201b4f7-6ab5-4df8-97a3-9bfa06043ce0
adoptationText: 'Daarnaast zijn er diverse organisaties actief aan de slag om de eerste stappen te zetten met NLX. Onder andere:'
organizationImages:
  - src: home/content/centric.svg
    alt: Centric
    url: https://www.centricpblo.nl/Common-Ground
  - src: home/content/utrecht.svg
    alt: Gemeente Utrecht
    url: https://utrecht.nl
  - src: home/content/logius.png
    alt: Logius
    url: http://www.logius.nl/
  - src: home/content/vng.svg
    alt: vng
    url: https://commonground.nl
  - src: home/content/enable-u.svg
    alt: Enable-U
    url: https://www.enable-u.nl/nlx-common-ground/
  - src: home/content/conduction.svg
    alt: Conduction
    url: https://www.conduction.nl/common-ground
  - src: home/content/open-gemeente.svg
    alt: Open gemeente initiatief
    url: https://opengem.nl/
---

## Ondersteunende organisaties

NLX wordt ontwikkeld in opdracht van VNG Realisatie. Door middel van praktijkproeven met gemeenten, ketenpartners en leveranciers wordt onderzocht wat daar allemaal bij komt kijken. Daarmee wordt de toegevoegde waarde, uitvoerbaarheid, opschaalbaarheid en impact van NLX verder in kaart gebracht. Gezamenlijk trekken we op om de Common Ground visie te realiseren. In het groeipact spreken de deelnemende partijen af dat ze die samenwerking aangaan. En ook: hoe ze die samenwerking vormgeven.
