---
icon: spy
---

### Makkelijker voldoen aan de privacywetgeving

Privacywetten zijn een belangrijk uitgangspunt bij de ontwikkeling van NLX. De software controleert of de data-eigenaar toestemming heeft gegeven aan de afnemer voor het gebruik van zijn data. Geen toestemming, geen data. Bovendien bewaart NLX wie, welke gegevens opvraagt en waar. Ook is in één overzicht duidelijk welke partij waartoe geautoriseerd is. Gemeenten die NLX gebruiken voor de uitwisseling van gegevens, voldoen daarmee aan de privacywet.  
