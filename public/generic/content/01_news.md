---
imageLink: generic/content/news.svg
imageAlt: A person giving a presentation on flipboard
newsLink1Text: Alle contactinformatie
newsLink1Href: /contact
newsLink2Text:
newsLink2Href:
---

## Nieuws en ontwikkelingen

Op de hoogte blijven van nieuws en ontwikkelingen rondom NLX? Regelmatig worden er (online) technische kennissessies georganiseerd. Iedere twee weken is de sprintreview waarin de voortgang van het scrum team besproken wordt.

Wilt u op de hoogte blijven, stuur dan een mail naar [support@nlx.io](mailto:support@nlx.io).
