---
linkText: Aan de slag met NLX Management
linkHref: /contact
imageLink: features/content/access.png
imageAlt: Screenshot van toegangsverzoeken voor een service.
imageText: Toegangsverzoeken afhandelen in NLX Management
---

## Authenticatie en autorisatie

Voor elke databevraging die via NLX wordt gedaan, wordt aan de leverende kant een controle gedaan op authenticatie en autorisatie. Pas als deze controle positief is, wordt een databevraging doorgelaten. De vragende kant is zelf verantwoordelijk voor een correcte invulling van de autorisatie binnen de eigen organisatie. Via de transactie log kan de legitimiteit van de bevragingen beoordeeld worden.
Voor authenticatie gebruikt NLX het <a href="https://www.logius.nl/diensten/pkioverheid" target="_blank">PKIoverheid-certificaat</a>: data-uitwisseling via NLX kan alleen maar met organisaties die een PKIoverheid-certificaat hebben. Het PKIoverheid-certificaat werkt als een digitaal paspoort. Logius verstrekt deze aan overheden en private partijen, zoals leveranciers. Bij de uitwisseling van data checkt NLX of de deelnemende partijen dit certificaat hebben.
NLX gebruikt een autorisatiemodel om te zorgen dat alleen bevoegde organisaties toegang krijgen tot gegevens. Hiermee kunnen overheden bescherming bieden tegen ongeoorloofde verstrekking van of ongeoorloofde toegang tot gegevens, waar artikel 32 lid 2 AVG hen toe verplicht.
