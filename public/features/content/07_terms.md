---
linkText: Bekijk NLX gebruiksvoorwaarden
linkHref: /contact
imageLink: features/content/voorwaarden.svg
imageAlt: Illustratie van zakelijk persoon.
imageText:
---

## Eerlijke gebruiksvoorwaarden

NLX is een stelsel waarmee gemeenten en andere overheidsorganisaties veilig en efficiënt data uitwisselen. Het bestaat uit software en afspraken, zoals gebruiksvoorwaarden. Organisaties die NLX willen gebruiken, gaan akkoord met de gebruiksvoorwaarden. Hierin is opgenomen waar organisaties aan moeten voldoen voor een accuraat en toekomstbestendig gebruik van NLX. Zoals het tijdig doorvoeren van updates en het beschikken over een PKIoverheid-certificaat.  
