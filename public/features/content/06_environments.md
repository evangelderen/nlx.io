---
linkText:
linkHref:
imageLink: features/content/environments.png
imageAlt: Illustratie van releaseflow van demo naar productie.
imageText:
---

## Omgevingen
NLX biedt een aantal omgevingen voor verschillende doeleinden. In de NLX <i>demo</i> omgeving kan een organisatie NLX testen: hoe installeer ik het, hoe koppel ik een applicatie aan een API, hoe maak ik mijn API beschikbaar voor anderen? Meestal zijn het ontwikkelaars die de documentatie doornemen en de try-me stappen volgen om daarna echte bevragingen over het demo netwerk van NLX te laten lopen. De stap erna is om het PKIoverheid-certificaat van de organisatie aan de NLX <i>pre-productie</i> omgeving te koppelen. In deze omgeving controleren organisaties of hun applicaties en/of API's klaar zijn om gegevens uit te wisselen met het PKIoverheid-certificaat. Het daadwerkelijk aansluiten op de NLX <i>productie</i> omgeving is de laatste stap die genomen moet worden om onderdeel te worden van het NLX ecosysteem en te profiteren van alle voordelen die dat heeft.
