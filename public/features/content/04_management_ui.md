---
linkText: Aan de slag met NLX Management
linkHref: /contact
imageLink: features/content/management_ui.png
imageAlt: Screenshot van de management interface.
imageText: Gebruikersinterface van NLX Management
---

## Gebruiksvriendelijke Management UI

Alle datastromen die via het NLX ecosysteem lopen, zijn inzichtelijk. Ze worden weergegeven in begrijpelijke overzichten. Hiermee houdt een beheerder de gezondheid van zijn NLX software in het oog en reageert wanneer dat wenselijk is. Toegangsverzoeken tot api's zijn in 1 oogopslag beschikbaar. Hierdoor blijven dit soort verzoeken geen dagen ongezien liggen. Ook zijn de datastromen per inway of outway in de eerste rudimentaire vorm te zien waarmee gecontroleerd kan worden of de toegewezen infrastructurele resources nog in balans zijn met de vraag. Uiteraard is de status van alle NLX componenten binnen de eigen organisatie inzichtelijk en wordt aandacht gevraagd voor huidige en toekomstige gebeurtenissen die van invloed zijn op de organisatie.
