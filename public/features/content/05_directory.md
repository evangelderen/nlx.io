---
linkText: Naar publieke directory
linkHref: https://directory.nlx.io
imageLink: features/content/directory.png
imageAlt: Screenshot van services in de directory.
imageText:
---

## Directory met beschikbare API'S

De NLX directory geeft inzicht in het gehele NLX-stelsel. Het geeft een overzicht van alle deelnemende organisaties, de beschikbare API's en ook de status van die API's. Op die manier is duidelijk waar de juiste data vandaan moet komen en kunnen de nieuwe koppelingen gelegd worden. Reeds bestaande koppelingen hebben de directory niet meer nodig. Voor ontwikkelaars is de documentatie van de beschikbare API's via de directory makkelijk te lezen.
