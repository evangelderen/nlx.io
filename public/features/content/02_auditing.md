---
linkText: Aan de slag met NLX Management
linkHref: /contact
imageLink: features/content/auditing.png
imageAlt: Screenshot van transactionslog in NLX Management.
imageText: Transactions log in NLX Management
---

## AVG proof logging en auditing

Bij het ontwikkelen van NLX zijn de eisen van de privacywetgeving een belangrijk uitgangspunt. Alle gegevens die via NLX worden uitgewisseld, worden op het niveau van de gebruikende organisatie geregistreerd. Er wordt gelogd wie bij welk systeem, welke gegevens heeft opgevraagd. Op een uniforme manier en op één plek. Dit levert een actueel en correct overzicht van het daadwerkelijk gegevensgebruik. Dit inzicht in gegevensstromen biedt de mogelijkheid om snel verdachte patronen te signaleren en kan worden gebruikt om verantwoording af te leggen over onder meer privacy en informatieveiligheid.
