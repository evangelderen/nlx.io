---
# For icon names check filenames in: src/icons
link1Icon: mail
link1Text: support@nlx.io
link1Href: mailto:support@nlx.io
link2Icon: gitlab
link2Text: Gitlab
link2Href: https://gitlab.com/commonground/nlx/nlx/-/issues
---

## Technische support

Bekijk de [technische documentatie](https://docs.nlx.io), of stel uw vraag via:
