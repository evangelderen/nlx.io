---
imageLink: contact/content/join.svg
imageAlt: Afbeelding van insteekmappen
link1Text: support@nlx.io
link1Href: mailto:support@nlx.io
link2Text:
link2Href:
---

## Meedoen met NLX?

NLX wordt momenteel door veel organisaties getest in de demo omgeving. Interesse? Volg de instructies in de <a href="https://docs.nlx.io/" target="_blank">technische documentatie</a>.

Ook wordt met een paar organisaties getest in de productie omgeving. Er is ruimte voor nog een aantal organisaties in deze fase. Interesse? Neem contact op via:
