---
imageLink: about/content/ladder.svg
imageAlt: Illustratie van mensen die samenwerken om hogerop te komen
---

## Achtergrond

Vanuit verschillende organisaties vonden gelijksoortige geluiden elkaar en ontkiemde zich een initiatief wat we zijn gaan leren kennen als de Common Ground beweging. Inmiddels is dat verankerd in de informatiekundige visie Common Ground. NLX, als landelijke integratie faciliteit is daar vanaf het prille begin integraal onderdeel van geweest. Het is de derde laag in het Common Ground <a href="https://commonground.nl/cms/view/12f73f0d-ae26-4021-ba52-849eef37d11f/de-common-ground-principes/03743740-a49f-48d8-9fc5-e24f86d748ed" target="_blank">vijflagenmodel</a> waarmee laagdrempelig toepassing en data gescheiden kan worden.

Een oplossing als NLX, die gegevens van overheidsorganisaties verbindt, is nog niet beschikbaar. Geïnspireerd door het bewezen succes van de Estlandse X-Road besloot een aantal gemeenten om onderzoek te doen naar een X-Road voor Nederland. Vandaar NLX.
NLX wordt open source ontwikkeld. Gemeenten, andere overheden en marktpartijen hebben daardoor de mogelijkheid om ontwikkelingen te volgen en mee te ontwikkelen.  

VNG Realisatie ondersteunt als vereniging de community Common Ground, waarin gemeenten samenwerken aan vereenvoudiging van het informatielandschap. Dat wat binnen Common Ground klaar is voor gebruik, wordt aan alle gemeenten beschikbaar gesteld. 
