FROM node:14.15.5-alpine AS build

# Install required build tools
RUN apk add --no-cache \
    build-base \
    python \
    openssl \
    make \
    git

# Copy only package.json & package-lock.json to make the dependency fetching step optional
COPY package.json \
     package-lock.json \
     .babelrc.js \
     jsconfig.json \
    /app/

COPY pages /app/pages/
COPY public /app/public/
COPY src /app/src/

WORKDIR /app

RUN npm install && \
    npm run build

# Copy static docs to alpine-based nginx container
FROM nginx:alpine

# Copy nginx configuration
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /app/out /usr/share/nginx/html

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx

USER appuser
