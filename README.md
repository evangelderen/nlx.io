# nlx.io
This repository contains the website [nlx.io](https://www.nlx.io).

## Content managmeent

The site can be content managed by editing the Markdown files in the `public/` folder.  
The files are sorted by page and section. The sections are ordered by a prefixed number for your convenience.

The file names have a 1:1 mapping with the code, so only change this in consultation with a developer.

**Technical lingo:**

- *Frontmatter*: key-value pairs at the top of the markdown file. Used to create anything but the main text of a section/page.  

### Icons

Due to a technical limitation icons are present in the `src/icons` folder (not in `public/`).  
If you want to change an icon, first make sure it exists in the `src/icons` folder and the `src/icons/index.js` file is updated to include it.

Then, in the markdown files, you may refer to the icon by its name. Eg: "tools" for `tools.svg`.

### Images

You can upload images to the proper `public/<page>/content` folder.

In frontmatter you can simply write the path to the image, without "public/". Example:

```
---
imageSrc: home/content/myimage.jpg
---
```

In markdown, use the image syntax with the same path:

```
![my image alt](home/content/myimage.jpg)
```

## Developing
The site is generated using [NextJS](https://nextjs.org).

Run `npm start` to start developing with live-reload.

### Page structure

- Files in `pages/` are the main page files
- These files are responsible for static data fetching and forward the data to the main render components in `src/pages/`
- Each page in `src/pages/` has a content folder with Markdown files that can be edited by content editors

The distinction between `pages/` and `src/pages` in required because:  

1. We want to group all files belonging to a page together
1. Nextjs recommends to only put pages in the pages folder
1. `export { default, getStaticProps } from 'src/pages/mypage'` is bugged, so getStaticProps needs to be in the page itself

### Guidelines

Render static pages where possible. So use [getStaticProps](https://nextjs.org/docs/basic-features/data-fetching#getstaticprops-static-generation)  and [getStaticPaths](https://nextjs.org/docs/basic-features/data-fetching#getstaticpaths-static-generation) for data fetching/routing. This will be aggregated build time. 

(TODO) For dynamic pages we may use a tool like [SWR](https://nextjs.org/docs/basic-features/data-fetching#fetching-data-on-the-client-side).

Currently we don't use server-side rendering.

### Tests

For this project we don't aim for 100% test coverage.  
Page rendering components etc do not need to be tested.

Only test (generic) components that are less than trivial.

## Building
To build a local static version of the site run:

```bash
npm run build
```

To run the built application:

```bash
npm run prod
```
