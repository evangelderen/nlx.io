// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
module.exports = {
  rootDir: '../src/',
  setupFilesAfterEnv: ['<rootDir>/../test-setup/index.js'],
}
