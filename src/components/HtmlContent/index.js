// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { string } from 'prop-types'
import styled from 'styled-components'

const Content = styled.div`
  h2 {
    margin-bottom: ${(p) => p.theme.tokens.spacing06};
  }
`

const HtmlContent = ({ content, ...props }) => (
  <Content dangerouslySetInnerHTML={{ __html: content }} {...props} />
)

HtmlContent.propTypes = {
  content: string,
}

HtmlContent.defaultProps = {
  content: '',
}

export default HtmlContent
