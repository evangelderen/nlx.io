// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'

export default styled.div`
  margin: ${(p) => p.theme.tokens.spacing06} 0;

  ${mediaQueries.xs`
    display: flex;
    flex-direction: column;
    align-items: center;
  `}

  & > * {
    margin: ${(p) => p.theme.tokens.spacing03};
  }
`
