// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import LinksWrapper from 'src/components/LinksWrapper'
import LinkButton from 'src/components/LinkButton'
import { Section, ImageCol, Image } from './index.styles'

const News = ({
  content,
  imageLink,
  imageAlt,
  newsLink1Text,
  newsLink1Href,
  newsLink2Text,
  newsLink2Href,
}) => (
  <Section alternate omitArrow>
    <Container>
      <Row>
        <Col width={[1, 1, 0.6666, 0.6666]}>
          <HtmlContent content={content} />

          <LinksWrapper>
            <LinkButton href={newsLink1Href} text={newsLink1Text} />
            <LinkButton href={newsLink2Href} text={newsLink2Text} />
          </LinksWrapper>
        </Col>

        <ImageCol width={[1, 1, 0.3333, 0.3333]}>
          <Image src={imageLink} alt={imageAlt} />
        </ImageCol>
      </Row>
    </Container>
  </Section>
)

News.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
  newsLink1Text: string,
  newsLink1Href: string,
  newsLink2Text: string,
  newsLink2Href: string,
}

export default News
