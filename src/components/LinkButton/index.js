// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Button } from '@commonground/design-system'
import Link from 'next/link'
import { Icon, IconExternalLink } from './index.styles'

const LinkButton = ({ href, text, ...props }) => {
  const isExternal = href && href.substring(0, 4) === 'http'

  return text && href ? (
    <Link href={href} passHref>
      <Button as="a" variant="link" {...props}>
        {text}
        {isExternal && <Icon as={IconExternalLink} inline />}
      </Button>
    </Link>
  ) : null
}

LinkButton.propTypes = {
  href: string,
  text: string,
}

export default LinkButton
