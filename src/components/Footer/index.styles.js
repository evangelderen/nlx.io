// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import {
  mobileNavigationHeight,
  mediaQueries,
} from '@commonground/design-system'
import LogoVng from './vng.svg'

export const Wrapper = styled.footer`
  padding: ${(p) => p.theme.tokens.spacing07} 0;
  background: #154967 url('generic/footer-bg-small.svg') no-repeat center bottom;

  ${mediaQueries.smDown`
    margin-bottom: ${mobileNavigationHeight};
  `}
`

export const FooterContent = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
  align-items: center;
  text-align: left;
`

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  padding: 0;
  margin: 0;
  list-style-type: none;

  ${mediaQueries.smUp`
    flex-direction: row;
  `}
`

export const Item = styled.li`
  padding: 0;
  margin: ${(p) => p.theme.tokens.spacing02} 0;

  ${mediaQueries.smUp`
    margin: 0 ${(p) => p.theme.tokens.spacing06} 0 0;
  `}

  a {
    color: #fff;
  }
`

export const StyledLogoVng = styled(LogoVng)`
  width: 100px;
`
