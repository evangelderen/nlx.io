// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { Section, ImageCol, Image } from './index.styles'

const Background = ({ content, imageLink, imageAlt }) => (
  <Section>
    <Container>
      <Row>
        <Col width={[1, 0.6666]}>
          <HtmlContent content={content} />
        </Col>
        <ImageCol width={[1, 0.3333]}>
          <Image src={imageLink} alt={imageAlt} />
        </ImageCol>
      </Row>
    </Container>
  </Section>
)

Background.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
}

export default Background
