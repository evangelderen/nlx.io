// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import Section, { SectionIntro } from 'src/components/Section'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import ButtonWrapper from 'src/components/ButtonWrapper'
import LinkButton from 'src/components/LinkButton'
import { getIcon } from 'src/icons'
import { List, Item, StyledIcon, StyledHtmlContent } from './index.styles'

const Solutions = ({
  content,
  button1Text,
  button1Link,
  button2Text,
  button2Link,
  ...props
}) => {
  const solutions = Object.entries(props)
    .filter(([key]) => key.substring(0, 8) === 'solution')
    .map(([, solution]) => solution)

  return (
    <Section>
      <Container>
        <CenterCol>
          <HtmlContent as={SectionIntro} content={content} />

          {solutions.length && (
            <List>
              {solutions.map((solution, i) => (
                <Item key={i}>
                  <StyledIcon as={getIcon(solution.icon)} />
                  <StyledHtmlContent content={solution.content} />
                </Item>
              ))}
            </List>
          )}

          <ButtonWrapper>
            <LinkButton
              variant="secondary"
              href={button1Link}
              text={button1Text}
            />
            <LinkButton
              variant="secondary"
              href={button2Link}
              text={button2Text}
            />
          </ButtonWrapper>
        </CenterCol>
      </Container>
    </Section>
  )
}

Solutions.propTypes = {
  content: string.isRequired,
  button1Text: string,
  button1Link: string,
  button2Text: string,
  button2Link: string,
}

export default Solutions
