// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'
import BaseSection from 'src/components/Section'

export const Section = styled(BaseSection)`
  background-image: url('home/overview-bg-small.svg');

  ${mediaQueries.mdUp`
    background-image: url('home/overview-bg-large.svg');
  `}
`
