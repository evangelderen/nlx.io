// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { SectionIntro } from 'src/components/Section'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import ButtonWrapper from 'src/components/ButtonWrapper'
import LinkButton from 'src/components/LinkButton'
import Diagram from './Diagram'
import { Section } from './index.styles'

const Overview = ({
  title,
  introText,
  content,
  button1Text,
  button1Link,
  button2Text,
  button2Link,
  ...diagramProps
}) => (
  <Section alternate>
    <Container>
      <CenterCol>
        <h2>{title}</h2>
        <SectionIntro>
          <p>{introText}</p>
        </SectionIntro>

        <Diagram {...diagramProps} />
      </CenterCol>

      <HtmlContent content={content} />

      <CenterCol>
        <ButtonWrapper>
          <LinkButton
            variant="secondary"
            href={button1Link}
            text={button1Text}
          />
          <LinkButton
            variant="secondary"
            href={button2Link}
            text={button2Text}
          />
        </ButtonWrapper>
      </CenterCol>
    </Container>
  </Section>
)

Overview.propTypes = {
  title: string.isRequired,
  introText: string.isRequired,
  content: string.isRequired,
  button1Text: string,
  button1Link: string,
  button2Text: string,
  button2Link: string,
}

export default Overview
