// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { IconCheck } from 'src/icons'

const DiagramMobile = ({ diagramA11yText, diagramFeaturesLinkHref }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="304"
      height="595"
    >
      <title>{diagramA11yText}</title>
      <defs>
        <rect
          id="overview-s-path-1"
          x="0"
          y="0"
          width="154"
          height="131"
          rx="16"
        />
        <filter
          x="-30.5%"
          y="-23.7%"
          width="161%"
          height="171.8%"
          filterUnits="objectBoundingBox"
          id="overview-s-filter-2"
        >
          <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feMorphology radius="2" in="SourceAlpha" result="shadowInner" />
          <feOffset dy="16" in="shadowInner" result="shadowInner" />
          <feComposite
            in="shadowOffsetOuter1"
            in2="shadowInner"
            operator="out"
            result="shadowOffsetOuter1"
          />
          <feGaussianBlur
            stdDeviation="13"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-s-path-3"
          x="24.305"
          y="23.138"
          width="106"
          height="60"
          rx="4"
        />
        <filter
          x="-13.2%"
          y="-16.7%"
          width="126.4%"
          height="146.7%"
          filterUnits="objectBoundingBox"
          id="overview-s-filter-4"
        >
          <feOffset dy="4" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-s-path-5"
          x="0"
          y="0"
          width="154"
          height="132"
          rx="16"
        />
        <filter
          x="-30.5%"
          y="-23.5%"
          width="161%"
          height="171.2%"
          filterUnits="objectBoundingBox"
          id="overview-s-filter-6"
        >
          <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feMorphology radius="2" in="SourceAlpha" result="shadowInner" />
          <feOffset dy="16" in="shadowInner" result="shadowInner" />
          <feComposite
            in="shadowOffsetOuter1"
            in2="shadowInner"
            operator="out"
            result="shadowOffsetOuter1"
          />
          <feGaussianBlur
            stdDeviation="13"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-s-path-7"
          x="0"
          y="0"
          width="106"
          height="60"
          rx="4"
        />
        <filter
          x="-13.2%"
          y="-16.7%"
          width="126.4%"
          height="146.7%"
          filterUnits="objectBoundingBox"
          id="overview-s-filter-8"
        >
          <feOffset dy="4" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
      </defs>
      <g
        id="overview-s-Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="overview-s-Artboard" transform="translate(-476 -279)">
          <g id="overview-s-Group" transform="translate(460 254)">
            <g id="overview-s-Group-2" transform="translate(16 25)">
              <g id="overview-s-Group-4-Copy-6" transform="translate(67)">
                <g id="overview-s-Group-Copy-4">
                  <path
                    id="overview-s-Line-2-Copy-18"
                    stroke="#757575"
                    strokeWidth="3"
                    strokeLinecap="square"
                    d="M77 68v84"
                  />
                  <g
                    id="overview-s-Group-2-Copy-2"
                    transform="translate(0 31.862)"
                  >
                    <g id="overview-s-Rectangle-Copy-13">
                      <use
                        fill="#000"
                        filter="url(#overview-s-filter-2)"
                        xlinkHref="#overview-s-path-1"
                      />
                      <rect
                        stroke="#9E9E9E"
                        strokeWidth="2"
                        strokeLinejoin="square"
                        x="1"
                        y="1"
                        width="152"
                        height="129"
                        rx="16"
                      />
                    </g>
                    <g id="overview-s-Rectangle">
                      <use
                        fill="#000"
                        filter="url(#overview-s-filter-4)"
                        xlinkHref="#overview-s-path-3"
                      />
                      <use fill="#FFF" xlinkHref="#overview-s-path-3" />
                    </g>
                    <text
                      id="overview-s-Applicatie"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                      fontSize="16"
                      fontWeight="normal"
                      fill="#212121"
                    >
                      <tspan x="44.132" y="57.138">
                        Applicatie
                      </tspan>
                    </text>
                  </g>
                  <g
                    id="overview-s-Group-6-Copy-2"
                    transform="translate(45 131.5)"
                  >
                    <rect
                      id="overview-s-Rectangle-Copy"
                      fill="#FFBC2C"
                      x="0"
                      y="0"
                      width="65"
                      height="42"
                      rx="4"
                    />
                    <text
                      id="overview-s-NLX"
                      fontFamily="SourceSansPro-Bold, Source Sans Pro"
                      fontSize="16"
                      fontWeight="bold"
                      fill="#212121"
                    >
                      <tspan x="18" y="25.5">
                        NLX
                      </tspan>
                    </text>
                  </g>
                  <text
                    id="overview-s-Organisatie-A-Copy"
                    fontFamily="SourceSansPro-Bold, Source Sans Pro"
                    fontSize="16"
                    fontWeight="bold"
                    fill="#212121"
                  >
                    <tspan x="30.5" y="16">
                      Organisatie A
                    </tspan>
                  </text>
                </g>
                <g id="overview-s-Group-Copy-5" transform="translate(0 410)">
                  <path
                    id="overview-s-Line-2-Copy-18"
                    stroke="#757575"
                    strokeWidth="3"
                    strokeLinecap="square"
                    d="M77 24v83"
                  />
                  <g
                    id="overview-s-Group-2-Copy-2"
                    transform="translate(0 10.862)"
                  >
                    <g id="overview-s-Rectangle-Copy-13">
                      <use
                        fill="#000"
                        filter="url(#overview-s-filter-6)"
                        xlinkHref="#overview-s-path-5"
                      />
                      <rect
                        stroke="#9E9E9E"
                        strokeWidth="2"
                        strokeLinejoin="square"
                        x="1"
                        y="1"
                        width="152"
                        height="130"
                        rx="16"
                      />
                    </g>
                    <g
                      id="overview-s-Group-3"
                      transform="translate(24.305 47.138)"
                    >
                      <g id="overview-s-Rectangle-Copy-2">
                        <use
                          fill="#000"
                          filter="url(#overview-s-filter-8)"
                          xlinkHref="#overview-s-path-7"
                        />
                        <use fill="#FFF" xlinkHref="#overview-s-path-7" />
                      </g>
                      <text
                        id="overview-s-Gegevens"
                        fontFamily="SourceSansPro-Regular, Source Sans Pro"
                        fontSize="16"
                        fontWeight="normal"
                        fill="#212121"
                      >
                        <tspan x="21.043" y="33.5">
                          Gegevens
                        </tspan>
                      </text>
                    </g>
                  </g>
                  <g id="overview-s-Group-6-Copy-2" transform="translate(45)">
                    <rect
                      id="overview-s-Rectangle-Copy"
                      fill="#FFBC2C"
                      x="0"
                      y="0"
                      width="65"
                      height="42"
                      rx="4"
                    />
                    <text
                      id="overview-s-NLX"
                      fontFamily="SourceSansPro-Bold, Source Sans Pro"
                      fontSize="16"
                      fontWeight="bold"
                      fill="#212121"
                    >
                      <tspan x="18" y="25.5">
                        NLX
                      </tspan>
                    </text>
                  </g>
                  <text
                    id="overview-s-Organisatie-B"
                    fontFamily="SourceSansPro-Bold, Source Sans Pro"
                    fontSize="16"
                    fontWeight="bold"
                    fill="#212121"
                  >
                    <tspan x="30.5" y="167">
                      Organisatie B
                    </tspan>
                  </text>
                </g>
                <path
                  id="overview-s-Line-2-Copy"
                  d="M77 189l7 14-6-.001v177l6 .001-7 14-7-14 6-.001v-177L70 203l7-14z"
                  fill="#757575"
                  fillRule="nonzero"
                />
              </g>
              <g id="overview-s-Group-3-Copy-3" transform="translate(163 208)">
                <g
                  id="overview-s-Group-16"
                  fill="#212121"
                  fontSize="14"
                  fontWeight="normal"
                >
                  <g id="overview-s-Group-10">
                    <IconCheck width="20" height="20" />
                    <text
                      id="overview-s-Autorisatie"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                    >
                      <tspan x="22" y="14">
                        Autorisatie
                      </tspan>
                    </text>
                  </g>
                  <g id="overview-s-Group-17" transform="translate(0 28)">
                    <IconCheck width="20" height="20" />
                    <text
                      id="overview-s-AVG-proof-logging-en"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                    >
                      <tspan x="22" y="14">
                        AVG proof logging{' '}
                      </tspan>{' '}
                      <tspan x="22" y="35">
                        en auditing
                      </tspan>
                    </text>
                  </g>
                  <g id="overview-s-Group-22" transform="translate(0 72)">
                    <IconCheck width="20" height="20" />
                    <text
                      id="overview-s-Eenvoudig-in-beheer"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                    >
                      <tspan x="22" y="14">
                        Eenvoudig in{' '}
                      </tspan>{' '}
                      <tspan x="22" y="35">
                        beheer
                      </tspan>
                    </text>
                  </g>
                </g>
                <a href={diagramFeaturesLinkHref} transform="translate(.5 96)">
                  <text
                    fontFamily="SourceSansPro-SemiBold, Source Sans Pro"
                    fontSize="16"
                    fontWeight="500"
                    fill="#0B71A1"
                  >
                    <tspan x="21.5" y="46">
                      Bekijk alle{' '}
                    </tspan>{' '}
                    <tspan x="21.5" y="66">
                      features
                    </tspan>
                  </text>
                </a>
              </g>
              <text
                id="overview-s-Snelle,-veilige-en-r"
                fontFamily="SourceSansPro-Regular, Source Sans Pro"
                fontSize="14"
                fontWeight="normal"
                fill="#212121"
              >
                <tspan x="6.231" y="231">
                  Snelle, veilige en{' '}
                </tspan>{' '}
                <tspan x=".12" y="252">
                  robuuste gateways
                </tspan>{' '}
                <tspan x="2.01" y="273">
                  in elke organisatie
                </tspan>
              </text>
              <path
                d="M140.586 317.046c-23.209 0-42.511 4.18-57.907 12.54-15.396 8.359-28.76 20.846-40.093 37.46"
                id="overview-s-Line-2-Copy-23"
                stroke="#9E9E9E"
                strokeLinecap="square"
                transform="scale(1 -1) rotate(-43 -776.748 0)"
              />
              <path
                d="M112.954 197.677c-5.684 0-10.411.752-14.182 2.257a26.781 26.781 0 00-9.818 6.743"
                id="overview-s-Line-2-Copy-25"
                stroke="#9E9E9E"
                strokeLinecap="square"
                transform="rotate(150 100.954 202.177)"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  )
}

DiagramMobile.propTypes = {
  diagramA11yText: string,
  diagramFeaturesLinkHref: string,
}

export default DiagramMobile
