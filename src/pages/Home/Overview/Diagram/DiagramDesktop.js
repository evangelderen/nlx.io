// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { IconCheck } from 'src/icons'

const DiagramDesktop = ({ diagramA11yText, diagramFeaturesLinkHref }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="696"
      height="457"
    >
      <title>{diagramA11yText}</title>
      <defs>
        <rect
          id="overview-l-path-1"
          x="0"
          y="0"
          width="198"
          height="131"
          rx="16"
        />
        <filter
          x="-23.7%"
          y="-23.7%"
          width="147.5%"
          height="171.8%"
          filterUnits="objectBoundingBox"
          id="overview-l-filter-2"
        >
          <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feMorphology radius="2" in="SourceAlpha" result="shadowInner" />
          <feOffset dy="16" in="shadowInner" result="shadowInner" />
          <feComposite
            in="shadowOffsetOuter1"
            in2="shadowInner"
            operator="out"
            result="shadowOffsetOuter1"
          />
          <feGaussianBlur
            stdDeviation="13"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-l-path-3"
          x="29.805"
          y="23.138"
          width="139"
          height="60"
          rx="4"
        />
        <filter
          x="-10.1%"
          y="-16.7%"
          width="120.1%"
          height="146.7%"
          filterUnits="objectBoundingBox"
          id="overview-l-filter-4"
        >
          <feOffset dy="4" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-l-path-5"
          x="0"
          y="0"
          width="198"
          height="132"
          rx="16"
        />
        <filter
          x="-23.7%"
          y="-23.5%"
          width="147.5%"
          height="171.2%"
          filterUnits="objectBoundingBox"
          id="overview-l-filter-6"
        >
          <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feMorphology radius="2" in="SourceAlpha" result="shadowInner" />
          <feOffset dy="16" in="shadowInner" result="shadowInner" />
          <feComposite
            in="shadowOffsetOuter1"
            in2="shadowInner"
            operator="out"
            result="shadowOffsetOuter1"
          />
          <feGaussianBlur
            stdDeviation="13"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <rect
          id="overview-l-path-7"
          x="0"
          y="0"
          width="139"
          height="60"
          rx="4"
        />
        <filter
          x="-10.1%"
          y="-16.7%"
          width="120.1%"
          height="146.7%"
          filterUnits="objectBoundingBox"
          id="overview-l-filter-8"
        >
          <feOffset dy="4" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"
            in="shadowBlurOuter1"
          />
        </filter>
      </defs>
      <g
        id="overview-l-Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="overview-l-Group-31" transform="translate(-405 -302.5)">
          <g id="overview-l-Group-34" transform="translate(405 302.5)">
            <g id="overview-l-Group-4" transform="translate(216)">
              <g id="overview-l-Group-Copy-4">
                <path
                  id="overview-l-Line-2-Copy-18"
                  stroke="#757575"
                  strokeWidth="3"
                  strokeLinecap="square"
                  d="M99 68v84"
                />
                <g
                  id="overview-l-Group-2-Copy-2"
                  transform="translate(0 31.862)"
                >
                  <g id="overview-l-Rectangle-Copy-13">
                    <use
                      fill="#000"
                      filter="url(#overview-l-filter-2)"
                      href="#overview-l-path-1"
                    />
                    <rect
                      stroke="#9E9E9E"
                      strokeWidth="2"
                      strokeLinejoin="square"
                      x="1"
                      y="1"
                      width="196"
                      height="129"
                      rx="16"
                    />
                  </g>
                  <g id="overview-l-Rectangle">
                    <use
                      fill="#000"
                      filter="url(#overview-l-filter-4)"
                      href="#overview-l-path-3"
                    />
                    <use fill="#FFF" href="#overview-l-path-3" />
                  </g>
                  <text
                    id="overview-l-Applicatie"
                    fontFamily="SourceSansPro-Regular, Source Sans Pro"
                    fontSize="16"
                    fontWeight="normal"
                    fill="#212121"
                  >
                    <tspan x="66.132" y="57.138">
                      Applicatie
                    </tspan>
                  </text>
                </g>
                <g
                  id="overview-l-Group-6-Copy-2"
                  transform="translate(67 131.5)"
                >
                  <rect
                    id="overview-l-Rectangle-Copy"
                    fill="#FFBC2C"
                    x="0"
                    y="0"
                    width="65"
                    height="42"
                    rx="4"
                  />
                  <text
                    id="overview-l-NLX"
                    fontFamily="SourceSansPro-Bold, Source Sans Pro"
                    fontSize="16"
                    fontWeight="bold"
                    fill="#212121"
                  >
                    <tspan x="18" y="25.5">
                      NLX
                    </tspan>
                  </text>
                </g>
                <text
                  id="overview-l-Organisatie-A-Copy"
                  fontFamily="SourceSansPro-Bold, Source Sans Pro"
                  fontSize="16"
                  fontWeight="bold"
                  fill="#212121"
                >
                  <tspan x="52.5" y="16">
                    Organisatie A
                  </tspan>
                </text>
              </g>
              <g id="overview-l-Group-Copy-5" transform="translate(0 272)">
                <path
                  id="overview-l-Line-2-Copy-18"
                  stroke="#757575"
                  strokeWidth="3"
                  strokeLinecap="square"
                  d="M99 24v83"
                />
                <g
                  id="overview-l-Group-2-Copy-2"
                  transform="translate(0 10.862)"
                >
                  <g id="overview-l-Rectangle-Copy-13">
                    <use
                      fill="#000"
                      filter="url(#overview-l-filter-6)"
                      href="#overview-l-path-5"
                    />
                    <rect
                      stroke="#9E9E9E"
                      strokeWidth="2"
                      strokeLinejoin="square"
                      x="1"
                      y="1"
                      width="196"
                      height="130"
                      rx="16"
                    />
                  </g>
                  <g
                    id="overview-l-Group-3"
                    transform="translate(29.805 47.138)"
                  >
                    <g id="overview-l-Rectangle-Copy-2">
                      <use
                        fill="#000"
                        filter="url(#overview-l-filter-8)"
                        href="#overview-l-path-7"
                      />
                      <use fill="#FFF" href="#overview-l-path-7" />
                    </g>
                    <text
                      id="overview-l-Gegevens"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                      fontSize="16"
                      fontWeight="normal"
                      fill="#212121"
                    >
                      <tspan x="37.543" y="33.5">
                        Gegevens
                      </tspan>
                    </text>
                  </g>
                </g>
                <g id="overview-l-Group-6-Copy-2" transform="translate(67)">
                  <rect
                    id="overview-l-Rectangle-Copy"
                    fill="#FFBC2C"
                    x="0"
                    y="0"
                    width="65"
                    height="42"
                    rx="4"
                  />
                  <text
                    id="overview-l-NLX"
                    fontFamily="SourceSansPro-Bold, Source Sans Pro"
                    fontSize="16"
                    fontWeight="bold"
                    fill="#212121"
                  >
                    <tspan x="18" y="25.5">
                      NLX
                    </tspan>
                  </text>
                </g>
                <text
                  id="overview-l-Organisatie-B"
                  fontFamily="SourceSansPro-Bold, Source Sans Pro"
                  fontSize="16"
                  fontWeight="bold"
                  fill="#212121"
                >
                  <tspan x="52.5" y="167">
                    Organisatie B
                  </tspan>
                </text>
              </g>
              <path
                id="overview-l-Line-2-Copy"
                d="M99 189l7.104 13.948-6.001.044.293 39.501 6-.044L99.5 256.5l-7.104-13.948 6-.045-.293-39.501-6 .045L99 189z"
                fill="#757575"
                fillRule="nonzero"
              />
            </g>
            <g
              id="overview-l-Group-16-Copy"
              transform="translate(470 182)"
              fontSize="16"
            >
              <g id="overview-l-Group-10" fill="#212121" fontWeight="normal">
                <IconCheck width="20" height="20" />
                <text
                  id="overview-l-Autorisatie"
                  fontFamily="SourceSansPro-Regular, Source Sans Pro"
                >
                  <tspan x="28" y="16">
                    Autorisatie
                  </tspan>
                </text>
              </g>
              <g
                id="overview-l-Group-17"
                transform="translate(0 32)"
                fill="#212121"
                fontWeight="normal"
              >
                <IconCheck width="20" height="20" />
                <text
                  id="overview-l-AVG-proof-logging-en"
                  fontFamily="SourceSansPro-Regular, Source Sans Pro"
                >
                  <tspan x="28" y="16">
                    AVG proof logging en auditing
                  </tspan>
                </text>
              </g>
              <g
                id="overview-l-Group-22"
                transform="translate(0 64)"
                fill="#212121"
                fontWeight="normal"
              >
                <IconCheck width="20" height="20" />
                <text
                  id="overview-l-Eenvoudig-in-beheer"
                  fontFamily="SourceSansPro-Regular, Source Sans Pro"
                >
                  <tspan x="28" y="16">
                    Eenvoudig in beheer
                  </tspan>
                </text>
              </g>
              <a href={diagramFeaturesLinkHref} transform="translate(.5 96)">
                <text
                  fontFamily="SourceSansPro-SemiBold, Source Sans Pro"
                  fontWeight="600"
                  fill="#0B71A1"
                >
                  <tspan x="27" y="29">
                    Bekijk alle features
                  </tspan>
                </text>
              </a>
            </g>
            <path
              d="M446.996 190.82c-14.064 14.341-28.574 23.559-43.532 27.653-14.957 4.094-27.941 10.21-38.951 18.347"
              stroke="#9E9E9E"
              strokeLinecap="square"
              transform="scale(1 -1) rotate(31 1176.764 0)"
            />
            <text
              fontFamily="SourceSansPro-Regular, Source Sans Pro"
              fontSize="16"
              fontWeight="normal"
              fill="#212121"
            >
              <tspan x="24.264" y="190">
                Snelle, veilige en
              </tspan>
              <tspan x="17.28" y="214">
                robuuste gateways
              </tspan>
              <tspan x="19.44" y="238">
                in elke organisatie
              </tspan>
            </text>
            <path
              d="M252.354 183.838c-23.632 0-43.286 1.812-58.962 5.436-15.676 3.624-29.283 9.037-40.822 16.24"
              id="overview-l-Line-2-Copy-6"
              stroke="#9E9E9E"
              strokeLinecap="square"
              transform="scale(1 -1) rotate(32 881.377 0)"
            />
            <path
              d="M255.354 225.838c-23.632 0-43.286 1.812-58.962 5.436-15.676 3.624-29.283 9.037-40.822 16.24"
              id="overview-l-Line-2-Copy-19"
              stroke="#9E9E9E"
              strokeLinecap="square"
              transform="scale(-1 1) rotate(-12 0 2191.514)"
            />
          </g>
        </g>
      </g>
    </svg>
  )
}

DiagramDesktop.propTypes = {
  diagramA11yText: string,
  diagramFeaturesLinkHref: string,
}

export default DiagramDesktop
