// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'

export const Wrapper = styled.div`
  position: relative;
  margin: 4rem 0 10rem;

  ${mediaQueries.sm`
    margin-bottom: 3rem;
  `}

  ${mediaQueries.mdUp`
    margin: ${(p) => p.theme.tokens.spacing11} 0;
  `}
`

export const Items = styled.ul`
  padding: 0;
  margin: 0;
  list-style-type: none;
`

export const Item = styled.li`
  margin: 0;
`
