// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import News from 'src/components/NewsSection'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import Symptoms from './Symptoms'
import Solutions from './Solutions'
import Overview from './Overview'
import Adoptation from './Adoptation'

const Home = ({ sections }) => (
  <div>
    <Head>
      <title>NLX - Homepage</title>
    </Head>

    <Header homepage />

    <main>
      <Introduction {...sections.introduction} />
      <Symptoms {...sections.symptoms} />
      <Solutions {...sections.solutions} />
      <Overview {...sections.overview} />
      <Adoptation {...sections.adoptation} />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
)

Home.propTypes = {
  sections: object.isRequired,
}

export default Home
