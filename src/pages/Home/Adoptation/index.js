// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string, arrayOf, shape } from 'prop-types'
import Section from 'src/components/Section'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import LinkButton from 'src/components/LinkButton'
import { LinkWrapper, AdoptationText, List, Item, Image } from './index.styles'

const Adoptation = ({
  content,
  mainLinkText,
  mainLinkHref,
  adoptationText,
  organizationImages,
}) => (
  <Section>
    <Container>
      <CenterCol>
        <HtmlContent content={content} />

        <LinkWrapper>
          <LinkButton href={mainLinkHref} text={mainLinkText} />
        </LinkWrapper>
      </CenterCol>

      {organizationImages.length && (
        <>
          <AdoptationText>{adoptationText}</AdoptationText>
          <List>
            {organizationImages.map(({ src, alt, url }, i) => (
              <Item key={i}>
                <a href={url} target="_blank" rel="noreferrer">
                  <Image src={src} alt={alt} />
                </a>
              </Item>
            ))}
          </List>
        </>
      )}
    </Container>
  </Section>
)

Adoptation.propTypes = {
  content: string.isRequired,
  mainLinkText: string,
  mainLinkHref: string,
  adoptationText: string.isRequired,
  organizationImages: arrayOf(
    shape({
      src: string.isRequired,
      alt: string,
    }),
  ),
}

export default Adoptation
