// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import styled from 'styled-components'

export const LinkWrapper = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};
`

export const AdoptationText = styled.p`
  text-align: center;
`

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0;
  margin: ${(p) => `${p.theme.tokens.spacing07} 0`};
  list-style-type: none;

  ${mediaQueries.smUp`
    flex-direction: row;
    align-items: unset;
    justify-content: space-evenly;
    flex-wrap: wrap;
  `}
`

export const Item = styled.li`
  padding: 0;
  margin: ${(p) => `${p.theme.tokens.spacing05} ${p.theme.tokens.spacing07}`};
`

export const Image = styled.img`
  display: block;
  max-width: 13rem;
  height: 3rem;
  filter: grayscale(100%);
  opacity: 0.8;

  ${mediaQueries.smUp`
    height: 4rem;
  `}

  a:focus > &, &:hover {
    filter: grayscale(0);
    opacity: 1;
  }
`
