// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'
import { Container } from 'src/components/Grid'
import BaseSection from 'src/components/Section'
import PersonaSvg from './jumbo/persona.svg'
import BuildingsSvg from './jumbo/buildings.svg'
import DarkBgSvg from './jumbo/dark-bg.svg'
import LightBgSvg from './jumbo/light-bg.svg'

const mobileBottomHeight = '122px'

export const Section = styled(BaseSection)`
  padding: 0 0 ${(p) => p.theme.tokens.spacing07};
`

export const Jumbotron = styled.div`
  position: relative;
  overflow: hidden;
  background: ${(p) => p.theme.homeGradientMobile};

  ${mediaQueries.smUp`
    min-height: 550px;
  `}

  ${mediaQueries.mdUp`
    min-height: 650px;
    background: ${(p) => p.theme.homeGradient};
  `}
`

export const MobileBottomBg = styled.div`
  height: ${mobileBottomHeight};
  background: linear-gradient(180deg, #e5f7ff 0%, #fff 55%);

  ${mediaQueries.smUp`
    display: none;
  `};
`

export const TextContainer = styled(Container)`
  position: relative;
  z-index: 6;
  margin-bottom: ${mobileBottomHeight};
  overflow: hidden;
`

export const Title = styled.h1`
  margin-top: ${(p) => p.theme.tokens.spacing06};

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing11};
    max-width: 39rem;
  `}
`

export const SubTitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  line-height: 175%;

  ${mediaQueries.mdUp`
    max-width: 27rem;
  `}
`

export const Persona = styled(PersonaSvg)`
  position: absolute;
  bottom: 0;
  left: 50%;
  z-index: 4;
  width: 384px;
  height: 252px;
  pointer-events: all;
  transform: translate(-100px, 0) scale(0.85);

  ${mediaQueries.smUp`
    transform: translate(-75px, -75px) scale(1);
  `}

  ${mediaQueries.mdUp`
    transform: translate(0, -150px) scale(1.25);
  `}
`

export const Buildings = styled(BuildingsSvg)`
  position: absolute;
  bottom: 0;
  left: 50%;
  z-index: 3;
  width: 444px;
  height: 268px;
  transform: translate(219px, -292px) scale(1.25);

  #dashedlines path {
    vector-effect: non-scaling-stroke;
    transform: rotate(1turn);
    animation: movingdash 5s infinite linear;
  }

  @keyframes movingdash {
    to {
      stroke-dashoffset: -100;
    }
  }
`

export const DarkBg = styled(DarkBgSvg)`
  position: absolute;
  bottom: 0;
  left: calc(50% - 700px);
  z-index: 2;
  width: 2000px;
  height: 400px;
`

export const LightBg = styled(LightBgSvg)`
  position: absolute;
  bottom: -150px;
  left: -390px;
  z-index: 1;
  width: 960px;
  height: 540px;

  ${mediaQueries.mdUp`
    bottom: -50px;
    left: calc(50% - 1080px);
  `}
`

export const Figure = styled.figure`
  position: relative;
  z-index: 5;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    flex-direction: column-reverse;
    margin: ${(p) =>
      `-40px ${p.theme.tokens.spacing07} ${p.theme.tokens.spacing09}`};
    /*transform: translateY(-2rem);*/
  `}

  ${mediaQueries.mdUp`
    margin-top: -${(p) => p.theme.tokens.spacing11};
  `}
`

export const FigCaption = styled.figcaption`
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  text-align: center;

  ${mediaQueries.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing05};
    margin-bottom: 0;
  `}
`

export const VideoFrame = styled.iframe`
  width: 420px;
  max-width: 100%;
  box-shadow: ${(p) => `rgba(0, 0, 0, 0.24) 0 8px 24px 0`};

  ${mediaQueries.mdUp`
    width: 640px;
    box-shadow: ${(p) => `rgba(0, 0, 0, 0.24) 0 24px 40px 0`};
  `}
`
