// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { string } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import { Media } from 'src/styling/media'
import {
  Section,
  Jumbotron,
  MobileBottomBg,
  Persona,
  Buildings,
  DarkBg,
  LightBg,
  TextContainer,
  Title,
  SubTitle,
  Figure,
  FigCaption,
  VideoFrame,
} from './index.styles'

const Introduction = ({
  siteTitle,
  siteSubTitle,
  videoCaption,
  videoId,
  videoCookieDisclaimer,
}) => (
  <Section omitArrow>
    <Jumbotron>
      <Media greaterThanOrEqual="md">
        <Buildings />
        <DarkBg />
      </Media>
      <LightBg />

      <TextContainer>
        <Row>
          <Col>
            <Title>{siteTitle}</Title>
            <SubTitle>{siteSubTitle}</SubTitle>
          </Col>
        </Row>
      </TextContainer>
      <MobileBottomBg />
      <Persona />
    </Jumbotron>

    <Container>
      <Figure>
        <FigCaption>
          {videoCaption}
          <br />
          <small>{videoCookieDisclaimer}</small>
        </FigCaption>
        <VideoFrame
          width="560"
          height="315"
          src={`https://www.youtube-nocookie.com/embed/${videoId}?rel=0`}
          title={videoCaption}
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></VideoFrame>
      </Figure>
    </Container>
  </Section>
)

Introduction.propTypes = {
  siteTitle: string.isRequired,
  siteSubTitle: string.isRequired,
  videoCaption: string.isRequired,
  videoId: string.isRequired,
  videoCookieDisclaimer: string.isRequired,
}

export default Introduction
