// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string, bool } from 'prop-types'
import HtmlContent from 'src/components/HtmlContent'
import LinkButton from 'src/components/LinkButton'
import {
  Section,
  Text,
  Container,
  Figure,
  Image,
  Figcaption,
} from './index.styles'

const Feature = ({
  featureName,
  content,
  linkHref,
  linkText,
  imageLink,
  imageAlt,
  imageText,
  wideImagePosition,
  narrowImage,
}) => (
  <Section feature={featureName}>
    <Container>
      <Text wideImagePosition={wideImagePosition} narrowImage={narrowImage}>
        <HtmlContent content={content} />
        <LinkButton href={linkHref} text={linkText} />
      </Text>

      <Figure wideImagePosition={wideImagePosition} narrowImage={narrowImage}>
        <Image src={imageLink} alt={imageAlt} />
        <Figcaption>{imageText}</Figcaption>
      </Figure>
    </Container>
  </Section>
)

Feature.propTypes = {
  featureName: string.isRequired,
  content: string.isRequired,
  linkText: string,
  linkHref: string,
  imageLink: string,
  imageAlt: string,
  imageText: string,
  wideImagePosition: string,
  narrowImage: bool,
}

Feature.defaultProps = {
  wideImagePosition: 'right',
  narrowImage: false,
}

export default Feature
