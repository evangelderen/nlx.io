// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'
import BaseSection from 'src/components/Section'
import { Container as BaseContainer } from 'src/components/Grid'

export const Section = styled(BaseSection)`
  padding: ${(p) => p.theme.tokens.spacing10} 0;

  ${(p) =>
    p.feature === 'auditing' &&
    `
    &::before {
      border-top-color: ${p.theme.colorAverageBlue};
    }
  `};

  ${(p) =>
    p.feature === 'access' &&
    `
    background: ${p.theme.colorAlternateSection};

    &::before {
      border-top-color: ${p.theme.tokens.colorBackground};
    }
  `};

  ${(p) =>
    p.feature === 'directory' &&
    `
    color: ${p.theme.tokens.colorBackground};
    background-color: ${p.theme.colorAverageBlue};

    a {
      color: #63C5FA;
    }

    svg {
      fill: ${p.theme.tokens.colorPaletteGray400};
    }

    &::before {
      border-top-color: ${p.theme.tokens.colorBackground};
    }
  `};

  ${(p) =>
    p.feature === 'directory' &&
    mediaQueries.mdUp`
      background-image: ${p.theme.gradientBlue};
  `}

  ${(p) =>
    p.feature === 'environments' &&
    `
    background-color: ${p.theme.colorAlternateSection};

    &::before {
      border-top-color: ${p.theme.colorAverageBlue};
    }
  `};
`

export const Container = styled(BaseContainer)`
  display: flex;
  flex-direction: column;

  ${mediaQueries.mdUp`
    flex-direction: row;
    padding: 0;
  `}
`

export const Text = styled.div`
  ${mediaQueries.mdUp`
    ${(p) => (p.narrowImage ? 'width: auto;' : 'width: 60%;')}
    
    ${(p) =>
      p.wideImagePosition === 'right'
        ? `
      margin: 0 ${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing05};
    `
        : `
      margin: 0 ${p.theme.tokens.spacing05} 0 ${p.theme.tokens.spacing07};
    `}
  `}

  ${mediaQueries.lgUp`
    width: auto;
  `}
`

export const Figure = styled.figure`
  order: -1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0;

  ${mediaQueries.mdUp`
    ${(p) =>
      p.narrowImage ? `overflow: visible; width: auto;` : 'width: 40%;'}

    ${(p) =>
      p.wideImagePosition === 'right'
        ? `
      order: unset;
      margin: 0 ${p.theme.tokens.spacing05} 0 ${p.theme.tokens.spacing07};
    `
        : `
      order: -1;
      margin: 0 ${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing05};
    `}
  `}

  ${mediaQueries.lgUp`
    width: auto;
  `}
`

export const Figcaption = styled.figcaption`
  margin-top: ${(p) => p.theme.tokens.spacing03};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  color: ${(p) => p.theme.tokens.colorPaletteGray600};
`

export const Image = styled.img`
  max-width: 200px;
  max-height: 180px;

  ${mediaQueries.sm`
    max-width: 300px;
  `}

  ${mediaQueries.md`
    width: 100%;
    min-width: 120px;
  `}

  ${mediaQueries.mdUp`
    max-width: 400px;
    max-height: none;
  `}
`
