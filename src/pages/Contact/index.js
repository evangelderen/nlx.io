// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import News from 'src/components/NewsSection'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import IntroBlocks from './IntroBlocks'
import Join from './Join'

const About = ({ sections }) => (
  <div>
    <Head>
      <title>NLX - Contact</title>
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <IntroBlocks blocks={sections.intro} />
      <Join {...sections.join} />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
)

About.propTypes = {
  sections: object.isRequired,
}

export default About
