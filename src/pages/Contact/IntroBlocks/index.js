// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { shape, string } from 'prop-types'
import Link from 'next/link'
import { Container, Row, Col } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { getIcon } from 'src/icons'
import {
  Section,
  Block,
  List,
  Item,
  StyledIcon,
  IconExternalLink,
} from './index.styles'

const IntroBlocks = ({ blocks }) => (
  <Section omitArrow>
    <Container>
      <Row>
        {Object.values(blocks).map(
          (
            {
              content,
              link1Icon,
              link1Text,
              link1Href,
              link2Icon,
              link2Text,
              link2Href,
            },
            i,
          ) => (
            <Col width={[1, 0.5]} key={i}>
              <Block>
                <HtmlContent content={content} />

                <List>
                  <Item>
                    <StyledIcon as={getIcon(link1Icon)} inline />
                    <Link href={link1Href}>{link1Text}</Link>
                  </Item>
                  {link2Href && link2Text && link2Icon && (
                    <Item>
                      <StyledIcon as={getIcon(link2Icon)} inline />
                      <Link href={link2Href}>{link2Text}</Link>
                      {link2Href.substring(0, 4) === 'http' && (
                        <IconExternalLink />
                      )}
                    </Item>
                  )}
                </List>
              </Block>
            </Col>
          ),
        )}
      </Row>
    </Container>
  </Section>
)

IntroBlocks.propTypes = {
  blocks: shape({
    content: string,
    link1Icon: string,
    link1Text: string,
    link1Href: string,
    link2Icon: string,
    link2Text: string,
    link2Href: string,
  }),
}

export default IntroBlocks
