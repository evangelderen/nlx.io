// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import Section from 'src/components/Section'
import HtmlContent from 'src/components/HtmlContent'
import LinksWrapper from 'src/components/LinksWrapper'
import LinkButton from 'src/components/LinkButton'
import { ImageCol, Image } from './index.styles'

const Join = ({
  content,
  imageLink,
  imageAlt,
  link1Text,
  link1Href,
  link2Text,
  link2Href,
}) => (
  <Section omitArrow>
    <Container>
      <Row>
        <ImageCol width={[1, 0.3333, 0.3333, 0.3333]}>
          <Image src={imageLink} alt={imageAlt} />
        </ImageCol>

        <Col width={[1, 0.6666, 0.6666, 0.6666]}>
          <HtmlContent content={content} />

          <LinksWrapper>
            {link1Href && link1Text && (
              <LinkButton href={link1Href} text={link1Text} />
            )}
            {link2Href && link2Text && (
              <LinkButton href={link2Href} text={link2Text} />
            )}
          </LinksWrapper>
        </Col>
      </Row>
    </Container>
  </Section>
)

Join.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
  link1Text: string,
  link1Href: string,
  link2Text: string,
  link2Href: string,
}

export default Join
