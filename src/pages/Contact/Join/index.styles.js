// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'
import { Col } from 'src/components/Grid'

export const ImageCol = styled(Col)`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Image = styled.img`
  max-width: 190px;
  margin: 0 auto;

  ${mediaQueries.mdUp`
    width: 100%;
    max-width: 250px;
  `}
`
