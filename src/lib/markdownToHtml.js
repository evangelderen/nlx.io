// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import remark from 'remark'
import html from 'remark-html'

export default async function markdownToHtml(markdown) {
  const result = await remark().use(html).process(markdown)
  return result.toString()
}
