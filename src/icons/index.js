// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import IconBox from './box.svg'
import IconBuilding from './building.svg'
import IconCheck from './check.svg'
import IconExternalLink from './external-link.svg'
import IconFileCopy from './file-copy-2.svg'
import IconGitlab from './gitlab.svg'
import IconGroup from './group-2.svg'
import IconHammer from './hammer.svg'
import IconHome from './home.svg'
import IconInfo from './info.svg'
import IconLightning from './flashlight.svg'
import IconMail from './mail.svg'
import IconOpenArm from './open-arm.svg'
import IconPlug from './plug.svg'
import IconRecycle from './recycle.svg'
import IconShieldCheck from './shield-check.svg'
import IconSpy from './spy.svg'
import IconTools from './tools.svg'

const icons = {
  box: IconBox,
  building: IconBuilding,
  check: IconCheck,
  externalLInk: IconExternalLink,
  fileCopy: IconFileCopy,
  gitlab: IconGitlab,
  group: IconGroup,
  hammer: IconHammer,
  home: IconHome,
  info: IconInfo,
  lightning: IconLightning,
  mail: IconMail,
  openArm: IconOpenArm,
  plug: IconPlug,
  recycle: IconRecycle,
  shieldCheck: IconShieldCheck,
  spy: IconSpy,
  tools: IconTools,
}

export {
  IconBox,
  IconBuilding,
  IconCheck,
  IconExternalLink,
  IconFileCopy,
  IconGitlab,
  IconGroup,
  IconHammer,
  IconHome,
  IconInfo,
  IconLightning,
  IconMail,
  IconOpenArm,
  IconPlug,
  IconRecycle,
  IconShieldCheck,
  IconSpy,
  IconTools,
}

export const getIcon = (icon) => {
  if (Object.keys(icons).includes(icon)) {
    // Above check makes it input-safe, so ignore eslint
    // eslint-disable-next-line security/detect-object-injection
    return icons[icon]
  }

  if (!process.env.NEXT_PUBLIC_PRODUCTION) {
    console.warn(`Icon "${icon}" not found`)
  }

  return icons.tools
}
